import request from "../utils/service";
///获取对象
export function selectByIdbigmap(param) {
  return request({
    url: `/ui/bigmapentity/selectById?id=` + param,
    method: "post",
  });
}
